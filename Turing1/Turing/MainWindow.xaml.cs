﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Turing
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        data data = new data();
        public MainWindow()
        {
            
            InitializeComponent();
            button.Click += work; // подпись на делегат, начинающий работу
            
        }



        private void button_Start(object sender, RoutedEventArgs e)
        {
            init();
        }
        private void work(object sender, RoutedEventArgs e)
        {
            data.count++;
            listBox.Items.Clear();
            logic Task = new logic();
            int count = 0;
            show(count, data.datalist, data.state, data.rool, data.pos);
            while (data.status && data.state != 0)
            {

                data = Task.task(data.datalist, data.status, data.count, data.position, data.state, data.commands, data.pos);
                if (data.count <= 0)
                {
                    MessageBox.Show("Предел ограницения по циклу", "count", MessageBoxButton.OK, MessageBoxImage.Error);
                    data.count = 100;
                    return;
                }

                count++;
                show(count, data.datalist, data.state, data.rool, data.pos);

            }
            if (data.state == 0)
            {
                MessageBox.Show("Ready");
                return;
            }

        }

        private void show(int count, List<char> list, int state, string rool, int position)
        {
            string line = "";
            line += count + ":";
            foreach (char b in list)
            {
                line += b;
            }
            line += " q" + state + " rool " + rool + "position" + position;
            listBox.Items.Add(line);
            listBox.UpdateLayout();

        }
        private void init()
        {
            data.commands = new List<List<string>>();
            data.rool = "";
            data.status = true;
            data.state = 1;
            try
            {
                data.count = Convert.ToInt32(count.Text);
            }
            catch
            {
                MessageBox.Show("проверьте поле итераций", "count", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
            }
            try
            {
                data.position = Convert.ToInt32(position.Text);
                data.pos = data.position;
            }
            catch
            {
                MessageBox.Show("проверьте поле позиции", "position", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
            }
            Listcheck();
            CommandCheck();
        }

        private void Listcheck()
        {
            Regex regex = new Regex(@"^[S][0-2,S]*[S]$");
            if (regex.IsMatch(inputline.Text))
            {
                List<char> line = new List<char>();
                foreach (char i in inputline.Text)
                {
                    line.Add(i);
                }
                data.datalist = line;
            }
            else
            {
                MessageBox.Show("проверьте поле ленты пример : \"S001122110S\"", "data", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
            }
        }

        private void CommandCheck()
        {
            Regex regex = new Regex(@"^[0-2,S][q]{1}[0-3]{1}[C,R,L]$");
            if (regex.IsMatch(command11.Text))
            {
                List<string> line = new List<string>();
                line.Add(command11.Text);
                data.commands.Add(line);
            }
            else
            {
                MessageBox.Show("проверьте команду 1:1", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
            }

            if (regex.IsMatch(command12.Text))
            {
                List<string> line = new List<string>();
                line.Add(command12.Text);
                data.commands.Add(line);
            }
            else
            {
                MessageBox.Show("проверьте команду 1:2 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command13.Text))
            {
                List<string> line = new List<string>();
                line.Add(command13.Text);
                data.commands.Add(line);
            }
            else
            {
                MessageBox.Show("проверьте команду 1:3 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command14.Text))
            {
                List<string> line = new List<string>();
                line.Add(command14.Text);
                data.commands.Add(line);
            }
            else
            {
                MessageBox.Show("проверьте команду 1:4 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command21.Text))
            {

                data.commands[0].Add(command21.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 2:1 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command22.Text))
            {

                data.commands[1].Add(command22.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 2:2 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command23.Text))
            {

                data.commands[2].Add(command23.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 2:3 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command24.Text))
            {

                data.commands[3].Add(command24.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 2:4 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command31.Text))
            {

                data.commands[0].Add(command31.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 3:1 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command32.Text))
            {

                data.commands[1].Add(command32.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 3:2 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command33.Text))
            {

                data.commands[2].Add(command33.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 3:3 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }

            if (regex.IsMatch(command34.Text))
            {

                data.commands[3].Add(command34.Text);
            }
            else
            {
                MessageBox.Show("проверьте команду 3:4 пример \"1q0L\"", "сommand", MessageBoxButton.OK, MessageBoxImage.Error);
                data.status = false;
                return;
            }
        }

        private void button_DEF1(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            inputline.Text = "S001122110S";
            position.Text = "2";
            count.Text = "101";
            command11.Text = "1q0L";
            command12.Text = "1q3R";
            command13.Text = "2q1R";
            command14.Text = "0q3R";

            command21.Text = "2q0C";
            command22.Text = "2q2R";
            command23.Text = "0q1R";
            command24.Text = "1q2R";

            command31.Text = "0q0R";
            command32.Text = "2q2L";
            command33.Text = "2q3C";
            command34.Text = "2q1L";
        }





        private void button_DEF2(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            inputline.Text = "S012012012S";
            position.Text = "2";
            count.Text = "101";
            command11.Text = "1q0L";
            command12.Text = "1q3R";
            command13.Text = "2q1R";
            command14.Text = "0q3R";

            command21.Text = "2q0C";
            command22.Text = "2q2R";
            command23.Text = "0q1R";
            command24.Text = "1q2R";

            command31.Text = "0q0R";
            command32.Text = "2q2L";
            command33.Text = "2q3C";
            command34.Text = "2q1L";
        }

        private void button_DEF3(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            inputline.Text = "S012012012S";
            position.Text = "2";
            count.Text = "101";
            command11.Text = "1q0L";
            command12.Text = "1q3R";
            command13.Text = "2q1R";
            command14.Text = "0q3R";

            command21.Text = "2q0C";
            command22.Text = "2q2R";
            command23.Text = "0q1R";
            command24.Text = "1q2R";

            command31.Text = "0q0R";
            command32.Text = "2q2L";
            command33.Text = "2q3C";
            command34.Text = "2q1L";
        }

        private void button_DEF4(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            inputline.Text = "S001122110S";
            position.Text = "2";
            count.Text = "103";
            command11.Text = "1q0L";
            command12.Text = "1q3R";
            command13.Text = "2q1R";
            command14.Text = "0q3R";

            command21.Text = "2q2C";
            command22.Text = "2q2R";
            command23.Text = "0q1R";
            command24.Text = "1q2R";

            command31.Text = "0q0R";
            command32.Text = "2q2L";
            command33.Text = "2q3C";
            command34.Text = "2q1L";
        }

        private void button1_Copy_Click(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            inputline.Text = "S001122110S";
            position.Text = "2";
            count.Text = "103";
            command11.Text = "1q0L";
            command12.Text = "1q3R";
            command13.Text = "2q1R";
            command14.Text = "0q3R";

            command21.Text = "2q2C";
            command22.Text = "2q2R";
            command23.Text = "0q1R";
            command24.Text = "1q2R";

            command31.Text = "0q0R";
            command32.Text = "2q2L";
            command33.Text = "2q3C";
            command34.Text = "2q1L";
        }

        private void button1_Copy2_Click_1(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            inputline.Text = "S001122110S";
            position.Text = "2";
            count.Text = "104";
            command11.Text = "1q0L";
            command12.Text = "1q3R";
            command13.Text = "2q1R";
            command14.Text = "0q3R";

            command21.Text = "0q2L";
            command22.Text = "2q2R";
            command23.Text = "0q1R";
            command24.Text = "1q2R";

            command31.Text = "0q0R";
            command32.Text = "2q2L";
            command33.Text = "2q3C";
            command34.Text = "2q1L";
        }

        private void Clean_Click(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();
            inputline.Text = position.Text =
             count.Text =
             command11.Text = command12.Text =
             command13.Text = command14.Text =
             command21.Text = command22.Text =
             command23.Text = command24.Text =
             command31.Text = command32.Text =
             command33.Text = command34.Text =
             String.Empty;




           
        }


    }
}
