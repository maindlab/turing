﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Turing
{
    class data
    {
        public int state; // текущее состояние машины
        public List<char> datalist; // текущая лента 
        public int count; // количество иттераций которые осталось машине
        public int position; // текущая позиция на ленте
        public int pos;
        public bool status; // показывает есть ли смысл продолжать дальше
        public string rool;
        public List<List<string>> commands = new List<List<string>>(); // двумерная таблица комманд
        /*
            1q0L   2q2C   1q1R 2q0R
            2q0R   2q1R   0q0L 2q1R
            Sq2L   1q0L   1q0L Sq2L
         */
    }
}
